var mymap = L.map('main_map').setView([-2.897680710096602, -79.00608792901039], 13);
var marker1 = L.marker([-2.897680710096602, -79.00608792901039]).addTo(mymap);
var marker2 = L.marker([-2.8980710096602, -79.008792901039]).addTo(mymap);
// var marker3 = L.marker([-2.890710096602, -79.00792901039]).addTo(mymap);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
}).addTo(mymap);

$.ajax({
   dataType: "json",
   url: "api/bicicletas",
   success: function (result) {
       console.log(result);
       result.bicicletas.forEach(function (bici) {
           L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
       })
   }
});

// marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
